package com.lannstark.resource;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ExcelDropdownMap {
    // key: fieldPath, value: String[]
    private Map<String, String[]> dropdownMap = new HashMap<>();

    public void put(String fieldPath, String[] dropdown) {
        this.dropdownMap.put(fieldPath, dropdown);
    }

    public String[] getDropdown(String fieldPath) {
        //
        return this.dropdownMap.getOrDefault(fieldPath, new String[]{});
    }
}
